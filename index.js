// console.log("Hello World!");

// Mock Database
let posts = [];

// Posts ID
let count = 1;


// Reactive DOM with JSON (CRUD Operation)

// ADD POST DATA
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	// Prevents the page from reloading. Also prevents the default behavior of our event.
	e.preventDefault();

	posts.push({
		// "id" property will be used for the unique ID of each posts.
		id: count,
		// "title" and "body" values will come from the "form-add-post" input elements.
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// count will increment everytime a new movie is posted.
	count++

	console.log(posts);
	alert("Movie Successfully Added!");

	showPosts();

});

// RETRIEVE POST
const showPosts = () => {

	// Create a variable that will contain all the posts
	let postEntries = "";

	// We will use forEach() to display each movie inside our mock database.
	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	});

	// To check what is stored in the postEntries variables.
	console.log(postEntries);

	// To assign the value of postEntries to the element with "div-post-entries" id.
	document.querySelector("#div-post-entries").innerHTML = postEntries

};


// EDIT POST DATA / Edit Button

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
};

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPosts(posts);
			alert("Successfully updated!");
			break;
		}
	}
});


const deletePost = (id) => {
	let getPost = document.querySelector(`#post-${id}`)
	let delThisPost;
	for(i = 0; i < posts.length; i++){
		if(getPost.id === `post-${posts[i].id}`){
			delThisPost = posts[i]
			break;
		}
	}
	function y (check) {
		return check == delThisPost
	}
	let indexOf = posts.findIndex(y)
	posts.splice(indexOf,1)
	showPosts(posts)
}


